import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { SearchService } from "./search.service";
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-search',
  templateUrl: "./search.component.html",
  styles: [],
  providers: [SearchService]
})
export class SearchComponent implements OnInit {

  
  // cars: Observable<any>
  cars:any = {};
  startAt = new Subject();
  endAt = new Subject();
  lastKeypress: number = 0;

  constructor(private searchService: SearchService) {
   
  }

  // getCars() {
  //   this.searchService.getCars()
  //     .subscribe(data => this.cars = data)
  // }

  ngOnInit() {
    this.searchService.getCars()
      .subscribe(cars => this.cars = cars)
  }

  getSearch(event?){
    this.searchService.getSearch()
      .subscribe(cars => this.cars = cars)
  }

  search($event){
    if($event.timeStamp - this.lastKeypress > 200){
      let q = $event.target.value
      this.startAt.next(q)
      this.endAt.next(q+"\uf8ff")
      console.log(q);
    }
    this.lastKeypress = $event.timeStamp
  }

  setSort(event: any) {
    this.searchService.setSort(event)
    this.getSearch()
  }

}
