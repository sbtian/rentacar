import { Injectable } from '@angular/core';
import { Car } from "./car.model";
import { HttpClient, HttpParams } from "@angular/common/http";
import { environment } from "../../environments/environment";


@Injectable({
  providedIn: 'root'
})
export class SearchService {

  readonly API_URL: string;
  readonly API_KEY: string;
  readonly API_SEARCH_URL: string;
  API_FACETS_URL: string;

  constructor(private http: HttpClient) {
    this.API_URL = environment.apiUrl;
    this.API_KEY = 'api_key='+environment.apyKey;
    this.API_SEARCH_URL = `${this.API_URL}search?${this.API_KEY}&facets=year,make,model&rows=50`;
    this.API_FACETS_URL = ''
  }
   
  getCars(){
    return this.http.get(this.API_SEARCH_URL)
  }

  getFacets(){
    return this.http.get(this.API_SEARCH_URL)
  }

  getCar(uid){
    let url = `${this.API_URL}listing/${uid}?${this.API_KEY}`
    return this.http.get(url)
  }

  getSearch(){
    let url = this.API_FACETS_URL
    console.log(url);
    return this.http.get(url)
  }

  setMake(make){
    let url = ''
    if(make!=''){
      url = `${this.API_SEARCH_URL}&make=${make}`
      this.API_FACETS_URL = url
      
    }
  }

  setModel(model){
    let url = ''
    if(model!=''){
      url = `${this.API_SEARCH_URL}&model=${model}`
      this.API_FACETS_URL = url
      
    }
  }

  setYear(year){
    let url = ''
    if(year!=''){
      url = `${this.API_SEARCH_URL}&year=${year}`
      this.API_FACETS_URL = url
      
    }
  }
  setSort(sort){
    let url = ''
    if(sort!='fav'){
      url = `${this.API_SEARCH_URL}&sort_by=price&sort_order=${sort}`
      this.API_FACETS_URL = url
      
    }else{
      url = `${this.API_SEARCH_URL}`
      this.API_FACETS_URL = url
    }
  }
}
