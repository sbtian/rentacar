export interface FacetItem {
    /**
     * Facet item
     */
    item?: string;
    /**
     * Facet item count
     */
    count?: number;
}