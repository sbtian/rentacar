import { NgModule } from '@angular/core';
import { ThemeComponent } from './theme.component';
import { Routes, RouterModule } from '@angular/router';
import { CanReadGuard } from "../core/can-read.guard";
import { AdminGuard } from "../core/admin.guard";
// import { AuthGuard } from '../auth/_guards/auth.guard';

const routes: Routes = [
    {
        'path': '',
        'component': ThemeComponent,
        // 'canActivate': [CanReadGuard],
        'children': [
            {
                'path': 'index',
                'loadChildren': '.\/pages\/default\/blank\/blank.module#BlankModule',
            },
            {
                'path': 'car:id',
                'loadChildren': '.\/pages\/default\/car_profile\/car_profile.module#CarProfileModule',
            },
            {
                'path': '',
                'redirectTo': 'index',
                'pathMatch': 'full',
            },
        ],
    },
    { 
        path: 'profile', 
        component: ThemeComponent, 
        canActivate: [CanReadGuard],
        'children': [
            {
                'path': '',
                'loadChildren': '.\/pages\/setup\/profile\/profile.module#ProfileModule',
            } 
        ],
    },
    { 
        path: 'config', 
        component: ThemeComponent, 
        canActivate: [AdminGuard],
        'children': [
            {
                'path': '',
                'loadChildren': '.\/pages\/setup\/config\/config.module#ConfigModule',
            } 
        ],
    },
    {
        'path': '**',
        'redirectTo': 'index',
        'pathMatch': 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ThemeRoutingModule { }