import { NgModule } from '@angular/core';
import { HeaderNavComponent } from './header-nav/header-nav.component';
import { DefaultComponent } from '../pages/default/default.component';
import { SetupComponent } from '../pages/setup/setup.component';
import { AsideNavComponent } from './aside-nav/aside-nav.component';
import { FooterComponent } from './footer/footer.component';
// import { QuickSidebarComponent } from './quick-sidebar/quick-sidebar.component';
import { ScrollTopComponent } from './scroll-top/scroll-top.component';
import { TooltipsComponent } from './tooltips/tooltips.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from "@angular/forms";

import { SearchComponent } from "../../search/search.component";

// import { HrefPreventDefaultDirective } from '../../_directives/href-prevent-default.directive';
// import { UnwrapTagDirective } from '../../_directives/unwrap-tag.directive';

@NgModule({
    declarations: [
        HeaderNavComponent,
        DefaultComponent,
        SetupComponent,
        AsideNavComponent,
        FooterComponent,
        // QuickSidebarComponent,
        ScrollTopComponent,
        TooltipsComponent,
        SearchComponent
        // HrefPreventDefaultDirective,
        // UnwrapTagDirective,
    ],
    exports: [
        HeaderNavComponent,
        DefaultComponent,
        SetupComponent,
        AsideNavComponent,
        FooterComponent,
        // QuickSidebarComponent,
        ScrollTopComponent,
        TooltipsComponent,
        SearchComponent
        // HrefPreventDefaultDirective,
    ],
    imports: [
        CommonModule,
        RouterModule,
        FormsModule
    ]
})
export class LayoutModule {
}