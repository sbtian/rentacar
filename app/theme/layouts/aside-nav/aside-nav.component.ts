import { Component, OnInit, ViewEncapsulation, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { SearchService } from "../../../search/search.service";
import { Helpers } from '../../../helpers';
import { Subject } from 'rxjs';

declare let mLayout: any;
@Component({
    selector: "app-aside-nav",
    templateUrl: "./aside-nav.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class AsideNavComponent implements OnInit, AfterViewInit {    

    @Output() onFilter = new EventEmitter();
    
    facets: any = {}
    make = new Array();
    model = new Array();
    year = new Array();

    startAt = new Subject();
    endAt = new Subject();
    constructor(private searchService:SearchService) {

    }
    ngOnInit() {
        this.searchService.getFacets()
            .subscribe(facets => this.facets = facets)
    }

    checkMake(event: any, val :any){
        if(val){
            this.make.push(event)
        }else{
            let pos = this.make.indexOf(event);
            this.make.splice(pos, 1);
        }
        this.searchService.setMake(this.make)
        this.onFilter.emit(null)   
    }
    checkModel(event: any, val :any){
        if (val) {
            this.model.push(event)
        } else {
            let pos = this.model.indexOf(event);
            this.model.splice(pos, 1);
        }
        this.searchService.setModel(this.model)
        this.onFilter.emit(null) 
    }
    checkYear(event: any, val :any){
        if (val) {
            this.year.push(event)
        } else {
            let pos = this.year.indexOf(event);
            this.year.splice(pos, 1);
        }
        this.searchService.setYear(this.year)
        this.onFilter.emit(null) 
    }

    ngAfterViewInit() {

        mLayout.initAside();

    }

}