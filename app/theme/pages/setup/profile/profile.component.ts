import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AuthService } from "../../../../core/auth.service";
import { Helpers } from '../../../../helpers';


@Component({
    selector: "app-profile",
    templateUrl: "./profile.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class ProfileComponent implements OnInit {


    constructor(public auth: AuthService) {

    }
    ngOnInit() {

    }

}