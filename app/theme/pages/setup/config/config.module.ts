import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ConfigComponent } from './config.component';
import { LayoutModule } from '../../../layouts/layout.module';
import { SetupComponent } from '../setup.component';

const routes: Routes = [
    {
        "path": "",
        "component": SetupComponent,
        "children": [
            {
                "path": "",
                "component": ConfigComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule
    ], exports: [
        RouterModule
    ], declarations: [
        ConfigComponent
    ]
})
export class ConfigModule {



}