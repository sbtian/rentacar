import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AuthService } from "../../../../core/auth.service";
import { Helpers } from '../../../../helpers';


@Component({
    selector: "app-config",
    templateUrl: "./config.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class ConfigComponent implements OnInit {


    constructor(public auth: AuthService) {

    }
    ngOnInit() {

    }

}